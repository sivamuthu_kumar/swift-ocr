//
//  ArrayExtension.swift
//  SwiftOCR
//
//  Created by Siva on 10/2/14.
//  Copyright (c) 2014 Vatsalya. All rights reserved.
//

import Foundation

extension Array {
    func combine(separator: String) -> String{
        var str : String = ""
        for (idx, item) in enumerate(self) {
            str += "\(item)"
            if idx < self.count-1 {
                str += separator
            }
        }
        return str
    }
}