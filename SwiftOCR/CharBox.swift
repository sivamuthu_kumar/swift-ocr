//
//  CharBox.swift
//  SwiftOCR
//
//  Created by Siva on 10/5/14.
//  Copyright (c) 2014 Vatsalya. All rights reserved.
//

import Foundation

class CharBox {
    
    var text : String
    var rect : CGRect
    
    init(text:String, rect:CGRect){
        self.text = text
        self.rect = rect
    }
}