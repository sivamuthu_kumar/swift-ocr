//
//  TextDetectionWrapper.h
//  SwiftOCR
//
//  Created by Siva on 9/26/14.
//  Copyright (c) 2014 Vatsalya. All rights reserved.
//

#ifndef __SwiftOCR__TextDetection__
#define __SwiftOCR__TextDetection__

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TextDetection : NSObject

-(id) initWithImage : (UIImage*)img;

-(void)populateChannels;

@property (strong, nonatomic) UIImage* image;

@end

#endif /* defined(__SwiftOCR__TextDetection__) */
