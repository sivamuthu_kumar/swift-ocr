//
//  UIImage+FixOrientation.h
//  SwiftOCR
//
//  Created by Siva on 10/7/14.
//  Copyright (c) 2014 Vatsalya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end